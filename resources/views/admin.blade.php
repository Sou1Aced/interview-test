@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as {{ Auth::user()->name }} on admin page!
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <form action="/update" method="POST">
                        {{ csrf_field() }}
                        <!-- {{ $errors->has('confirmation') ? 'is-danger' : ''}} -->
                        <label>User ID:</label>
                        <input type="input" name="id" class="form-control" value="" required>
                        <label>Approval Choice:</label>
                        <select name="approval" >
                            <option value="1" selected>Approved</option>
                            <option value="0">Unapproved</option>
                        </select>
                        <input type="submit" value="Submit">

                    </form>
                </div>

                <div class="card-body">
                    <table>
                        <tr>
                            <th>ID</th><th>Username</th><th>Email</th><th>Approval</th><th>Is Admin</th>
                        </tr>
                        <?php
                            use App\Http\Controllers\HomeController;
                            $users = HomeController::getAllUsers();

                            $i = 0;
                            foreach ($users as $u) {
                                echo "<tr>";
                                echo "<td>" . $u->id . "</td><td>" . $u->name . "</td><td>" . $u->email . "</td><td>" 
                                    . $u->approved . "</td><td>" . $u->is_admin;
                                echo "</tr>";

                                $i++;
                            }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection