<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');

        if(auth()->user()->is_admin == 1){
            return view('admin');
        }else if (auth()->user()->approved == 1){
            return view('approved');
        }else{
            return view('unapproved');
        }
    }

    public static function getAllUsers(){
        //should not pull in any admins
        return User::where('is_admin', 0)->get();
    }

    public function update(){
        $user = User::where('id', $_REQUEST['id'])->first();
        $user->approved = $_REQUEST['approval'];
        $user->save();
        header("Location: /home");
        exit();
    }
}
