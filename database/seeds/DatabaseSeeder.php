<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            // 'username' => 'Str::random(10)',
            'name' => 'admin',
            // 'password' => Hash::make('password'),
            'email' => 'admin@yahoo.com',
            'email_verified_at' => null,
            'password' => Hash::make('admin'),
            'approved' => true,
            'is_admin' => true,
            'remember_token' => null
        ]);
    }
}
